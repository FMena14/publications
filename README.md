### **Francisco Mena** links
* __[Google Scholar](https://scholar.google.com/citations?user=KMyi5JQAAAAJ)__
* __[ResearchGate](https://www.researchgate.net/profile/Francisco-Mena-3)__
* __[ORCID 0000-0002-5004-6571](https://orcid.org/0000-0002-5004-6571)__
* __[LinkedIN](https://www.linkedin.com/in/fmenat/)__
* __[Zotero](https://www.zotero.org/fmenat)__
* __[Gitlab](https://gitlab.com/fmena14/)__ & __[Github](https://github.com/fmenat)__
* __[Twitter](https://twitter.com/fmenat14)__

> Email: <img src="https://render.githubusercontent.com/render/math?math=\text{francisco.menat@usm.cl}">

# List of Publications
---

* [2021, journal] **Francisco Mena**, Patricio Olivares, Margarita Bugueño, Gabriel Molina, Mauricio Araya. *On the Quality of Deep Representations for Kepler Light Curves Using Variational Auto-Encoders*. Special Issue *Machine Learning and Signal Processing* of Signals.  __[10.3390/signals2040042](https://doi.org/10.3390/signals2040042)__
    > [Manuscript](./manuscripts/2021_Signals_VAE.pdf)  
    > [Supplementary Poster](./posters/2020_VAE.pdf)  

* [2021, conference proceeding] Ricardo Ñanculef, **Francisco Mena**, Antonio Macaluso, Stefano Lodi, Claudio Sartori. *Self-Supervised Bernoulli Autoencoders for Semi-Supervised Hashing*. Iberoamerican Congress on Pattern Recognition (CIARP), Springer. __[10.1007/978-3-030-93420-0_25](https://doi.org/10.1007/978-3-030-93420-0_25)__
    > [Manuscript](./manuscripts/Preprint_SelfB-VAE.pdf)  
    > [Presentation](./presentations/2021_CIARP_SSBVAE.pdf)  

* [2021, journal] Margarita Bugueño, Gabriel Molina, **Francisco Mena**, Patricio Olivares, Mauricio Araya. *Harnessing the power of CNNs for unevenly-sampled light-curves using Markov Transition Field*. Astronomy and Computing. __[10.1016/j.ascom.2021.100461](https://doi.org/10.1016/j.ascom.2021.100461)__
    > [Manuscript](./manuscripts/2021_AC_MTF.pdf)  
    > [Supplementary Poster](./posters/2020_MTF.pdf)  

* [2020, journal] **Francisco Mena**, Ricardo Ñanculef, Carlos Valle. *Interpretable and effective hashing via Bernoulli variational auto-encoders*. Intelligent Data Analysis. __[10.3233/IDA-200013](https://dx.doi.org/10.3233/IDA-200013)__
    > [Manuscript](./manuscripts/2020_IDA_BVAE.pdf)  

* [2020, journal] **Francisco Mena**, Ricardo Ñanculef, Carlos Valle. *Collective annotation patterns in learning from crowds*. Intelligent Data Analysis. __[10.3233/IDA-200009](https://dx.doi.org/10.3233/IDA-200009)__
    > [Manuscript](./manuscripts/2020_IDA_CMM.pdf)  
    
* [2020, short-paper] Gabriel Molina, **Francisco Mena**, Margarita Bugueño, Mauricio Solar. *Can we interpret machine learning? An analysis of exoplanet detection problem*. Astronomical Data Analysis Software and Systems (ADASS), ASPCS. __[link](https://ui.adsabs.harvard.edu/abs/2020ASPC..527..183M/abstract)__
    > [Manuscript](./manuscripts/2019_ADASS_Exoplanet.pdf)  
    > [Poster](./posters/2019_ADASS_Exoplanet.pdf) 

* [2019, journal] **Francisco Mena** & Margarita Bugueño, Mauricio Araya. *Classical machine learning techniques in the search of extrasolar planets*. CLEI Electronic Journal. __[10.19153/cleiej.22.3.3](https://dx.doi.org/10.19153/cleiej.22.3.3)__
    > [Manuscript](./manuscripts/2019_CLEIEJ_Exoplanet.pdf)  
    
* [2019, conference proceeding] **Francisco Mena**, Ricardo Ñanculef. *Revisiting machine learning from crowds a mixture model for grouping annotations*. Iberoamerican Congress on Pattern Recognition (CIARP), Springer. __[10.1007/978-3-030-33904-3\_46](https://dx.doi.org/10.1007/978-3-030-33904-3\_46)__
    > [Manuscript](./manuscripts/2019_CIARP_CMM.pdf)  
    > [Presentation](./presentations/2019_CIARP_CMM.pdf)  

* [2019, conference proceeding] **Francisco Mena**, Ricardo Ñanculef. *A binary variational autoencoder for hashing*. Iberoamerican Congress on Pattern Recognition (CIARP), Springer. __[10.1007/978-3-030-33904-3\_12](https://dx.doi.org/10.1007/978-3-030-33904-3\_12)__
    > [Manuscript](./manuscripts/2019_CIARP_BVAE.pdf)  
    > [Presentation](./presentations/2019_CIARP_BVAE.pdf)  

* [2018, conference proceeding] Margarita Bugueno, **Francisco Mena**, Mauricio Araya. *Refining exoplanet detection using supervised learning and feature engineering*. Latin American Computer Conference (CLEI), IEEE. __[10.1109/CLEI.2018.00041](https://dx.doi.org/10.1109/CLEI.2018.00041)__
    > [Manuscript](./manuscripts/2018_CLEI_Exoplanet.pdf)  
    > [Presentation](./presentations/2018_SLIOA-CLEI_Exoplanet.pdf)  
    > [Supplementary Poster](./posters/2018_ChileWIC_exoplanet.pdf)  
    > [Supplementary Presentation](./presentations/2019_DI-UTFSM_Exoplanet.pdf) 
    
    
## Results Report
---
* [2019] *Evaluating Bregman Divergences for Probability Learning from Crowd*. arXiv preprint arXiv:1901.10653.
    > [Manuscript](./manuscripts/results_2018_Bdiv.pdf)  
    > __[Code](https://github.com/FMena14/ML_usm/tree/master/Trabajo_ANN)__

---
[UP](#list-of-publications)

> Go to [Organized](./organization.md) details.